﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(TwitchIRC))]
public class TwitchChatStream : MonoBehaviour
{
	private TwitchIRC IRC;

	//when message is recieved from IRC-server or our own message.
	void OnChatMsgRecieved(string msg)
	{
		//parse from buffer.
		int msgIndex = msg.IndexOf("PRIVMSG #");
		string msgString = msg.Substring(msgIndex + IRC.channelName.Length + 11);
		string user = msg.Substring(1, msg.IndexOf('!') - 1);

		DigestMessage(user, msgString);
	}
	void DigestMessage(string userName, string msgString)
	{
		//Debug.Log("User: " + userName);
		//Debug.Log("msg: " + msgString);

		LanguageParser.getInstance().ParseChat(userName, msgString);
	}

	// Use this for initialization
	void Start()
	{
		IRC = this.GetComponent<TwitchIRC>();
		//IRC.SendCommand("CAP REQ :twitch.tv/tags"); //register for additional data such as emote-ids, name color etc.
		IRC.messageRecievedEvent.AddListener(OnChatMsgRecieved);
	}
}
