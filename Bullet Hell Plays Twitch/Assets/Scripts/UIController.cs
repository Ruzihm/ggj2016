﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class UIController : MonoBehaviour {

	/*public struct ScoreboardEntry : IComparer{
		public string username;
		public float score;

		public ScoreboardEntry (string u, float s) {
			username = u;
			score = s;
		}

		public int Compare (object x, object y) {
			ScoreboardEntry left = (ScoreboardEntry) x;
			ScoreboardEntry right = (ScoreboardEntry) y;

			return right.score - left.score;
		}
	}*/

	[SerializeField]
	private AudioSource enterSource;

	[SerializeField]
	private AudioSource gameOverSource;

	[SerializeField]
	private Text boardTitle;

	[SerializeField]
	private string titleFormat = "   Leaderboard ({0} active players)";

	[SerializeField]
	private Text[] scoreFields;

	[SerializeField]
	private Text[] resultScoreFields;

	[SerializeField]
	private string scoreFormat = "   {0}. {1:0000} [{2}]";

	public Dictionary<string, float> scores = new Dictionary<string, float>();
	public SortedList scoreboard = new SortedList();

	public GameObject titleScreen;
	public GameObject scoreBoard;
	public GameObject resultScreen;

	public InputField usernameInput;
	public InputField oauthInput;

	public TwitchIRC irc;

	public Text resultsText;
	public Text resultBoardText;

	public Toggle isPlayerAIControlled;
	public Toggle isEnemyAIControlled;

	void Awake () {
		ShowTitleScreen();
	}

	void ShowTitleScreen () {
		titleScreen.SetActive(true);
		scoreBoard.SetActive(false);
		resultScreen.SetActive(false);
	}

	void DismissTitleScreen () {
		if (!string.IsNullOrEmpty(usernameInput.text))  {
			irc.oauth = oauthInput.text;
			irc.nickName = usernameInput.text;
			irc.channelName = usernameInput.text;
		}
		irc.enabled = true;

		//Debug.Log(isPlayerAIControlled.isOn);
		EnemyController.getInstance().aiControlled = isEnemyAIControlled.isOn;
		PlayerController.getInstance().aiControlled = isPlayerAIControlled.isOn;
		//Debug.Log(isEnemyAIControlled.isOn);
		EnemyController.getInstance().Reset();
		PlayerController.getInstance().Reset();

		EnemyController.getInstance().enabled = true;
		PlayerController.getInstance().enabled = true;
		titleScreen.SetActive(false);
		resultScreen.SetActive(false);
		scoreBoard.SetActive(true);
		UpdateScoreBoard();
	}

	public IEnumerator ShowResultsScreen (bool playerWin) {
		EnemyController.getInstance().StopAllShooting();
		LanguageInterface.getInstance().Renew();

		yield return new WaitForSeconds(3f);
		gameOverSource.Play();
		titleScreen.SetActive(false);
		resultsText.text = playerWin ? "Player Wins!" : "The Chat Wins!";
		resultBoardText.text = scores.Count > 0 ? "Top Daemonic Masters" : "Flawless Victory!";
		resultScreen.SetActive(true);
		scoreBoard.SetActive(false);

		if (PlayerController.getInstance().aiControlled) {
			yield return new WaitForSeconds(5f);
			enterSource.Play();
			DismissResultsScreen();
		}
	}

	public void DismissResultsScreen () {
		titleScreen.SetActive(false);
		resultScreen.SetActive(false);
		scoreBoard.SetActive(true);

		//TODO: reset queue for enemy actions.
                                 		//TODO: reset shooter behavior.

		scores.Clear();
		scoreboard.Clear();
		UpdateScoreBoard();

		//clear out shooters before this.
		GameObject[] remainingBullets = GameObject.FindGameObjectsWithTag("Bullet");
		for (int i = 0; i < remainingBullets.Length; i++) {
			if (remainingBullets[i] != null) {
				remainingBullets[i].SetActive(false);
				Destroy(remainingBullets[i]);
			}
		}

		EnemyController.getInstance().Reset();
		PlayerController.getInstance().Reset();
	}

	// Use this for initialization
	void Start () {
		UpdateScoreBoard();
	}

	void Update () {
		if (Input.GetButtonDown("Submit") && titleScreen.activeSelf) {
			enterSource.Play();
			DismissTitleScreen();
		}

		if (Input.GetButtonDown("Submit") && resultScreen.activeSelf) {
			enterSource.Play();
			DismissResultsScreen();
		}

		if (Input.GetKeyDown(KeyCode.Escape)) {
			#if UNITY_EDITOR
			UnityEditor.EditorApplication.isPlaying = false;
			#else
			Application.Quit();
			#endif
		}
	}
	
	// Update is called once per frame
	void UpdateScoreBoard () {
		for (int i = 0; i < scoreFields.Length; i++) {
			scoreFields[i].text = i < scoreboard.Count ? string.Format(scoreFormat, i+1, scoreboard.GetKey(scoreboard.Count - (1 + i)), scoreboard.GetByIndex(scoreboard.Count - (1 + i))) : string.Empty;
			resultScoreFields[i].text = i < scoreboard.Count ? string.Format(scoreFormat, i+1, scoreboard.GetKey(scoreboard.Count - (1 + i)), scoreboard.GetByIndex(scoreboard.Count - (1 + i))) : string.Empty;
		}

		//Debug.Log("scoreboard.Count" + scoreboard.Count);
		scoreBoard.SetActive(scoreboard.Count > 0);
	}

	public void AddScoreEntry (string name, float damageDealt) {
		if (name == EnemyController.defaultUsername) {
			return;
		}

		if (!scores.ContainsKey(name)) {
			scores.Add(name, 0f);
		}

		scores[name] = scores[name] + damageDealt;
		SortScores();
	}

	/* Called when a score change occurs */
	public void SortScores () {
		scoreboard.Clear();

		foreach (string key in scores.Keys) {
			scoreboard.Add(scores[key], key);
		}
		UpdateScoreBoard();
	}
}
