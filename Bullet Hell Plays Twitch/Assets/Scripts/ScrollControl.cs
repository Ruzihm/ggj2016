﻿using UnityEngine;
using System.Collections;

public class ScrollControl : MonoBehaviour {

	OffsetScroller[] scrolls;

	public AnimationCurve slowDown;

	public float slowDownTime = 5f;
	private float slowDownTimeStart = 0f;

	public AnimationCurve speedUp;

	public float speedUpTime = 5f;
	private float speedUpTimeStart = 0f;

	void Awake () {
		scrolls = GetComponentsInChildren<OffsetScroller>();
	}

	public void StartSlowDown () {
		StartCoroutine(SlowDown());
	}

	IEnumerator SlowDown () {
		slowDownTimeStart = Time.time;
		while (Time.time - slowDownTimeStart <= slowDownTime) {
			for (int i = 0; i < scrolls.Length; i++) {
				float t = (Time.time - slowDownTimeStart) / slowDownTime;
				//Debug.Log("t " + t);
				//Debug.Log("slowDown.Evaluate(t) " + slowDown.Evaluate(t));
				scrolls[i].scrollSpeed = scrolls[i].originalScrollSpeed * (1 - slowDown.Evaluate(t));
				if (i == 0) {
					Debug.Log("scroll speed " + scrolls[i].scrollSpeed);
				}
			}
			yield return null;
		}

		for (int i = 0; i < scrolls.Length; i++) {
			scrolls[i].scrollSpeed = 0f;
		}
	}

	/*IEnumerator Start () {
		yield return new WaitForSeconds(4f);
		StartSlowDown();
	}*/

}