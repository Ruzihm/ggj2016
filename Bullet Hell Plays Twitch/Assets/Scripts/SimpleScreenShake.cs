﻿using UnityEngine;
using System.Collections;

public class SimpleScreenShake : MonoBehaviour {

	static SimpleScreenShake instance; // dumb but yes because game jam.
	Transform cachedTransform;


	void Awake () {
		instance = this;
		cachedTransform = transform;
	}

	//local base == Vector3.zero

	IEnumerator ScreenShake (float xIntensity, float yIntensity, float zIntensity, float duration) {
		float startTime = Time.time;
		while (Time.time - startTime < duration)
		{
			//Debug.Log("shaket");
			cachedTransform.localPosition = Vector3.Lerp(Vector3.zero, new Vector3(Random.Range(-xIntensity, xIntensity), Random.Range(-yIntensity, yIntensity), Random.Range(-zIntensity, zIntensity)), Time.deltaTime);
			yield return null;
		}
		cachedTransform.localPosition = Vector3.zero;
	}

	public static void DoScreenShake (float xIntensity, float yIntensity, float zIntensity, float duration) {
		if (instance != null) {
			instance.StopAllCoroutines();
			if (instance.gameObject.activeSelf)
			{
				instance.StartCoroutine(instance.ScreenShake(xIntensity, yIntensity, zIntensity, duration));
			}
		}
	}

	public static void StopAllScreenShake () {
		if (instance != null) {
			instance.StopScreenShake();
		}
	}

	void StopScreenShake () {
		StopAllCoroutines();
		cachedTransform.localPosition = Vector3.zero;
	}

	/*void Update () {
		if (Input.GetKeyDown(KeyCode.M)) {
			SimpleScreenShake.DoScreenShake(10f, 0f, 10f, 1f);
		}
	}*/
}