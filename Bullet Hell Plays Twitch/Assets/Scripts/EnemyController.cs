﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class EnemyController : MonoBehaviour {

	// 0 = tl
	// 1 = tm
	// 2 = tr
	// 3 = bl
	// 4 = bm
	// 5 = br

	public GameObject Shooter_Center_Normal;
	public GameObject Shooter_Left_Normal;
	public GameObject Shooter_Center_Wave;
	public GameObject Shooter_Left_Wave;
	public GameObject Shooter_Center_Mine;
	public GameObject Shooter_Left_Mine;
	public GameObject Shooter_Center_Seeker;
	public GameObject Shooter_Left_Seeker;
	public GameObject Shooter_Center_Laser;
	public GameObject Shooter_Left_Laser;

	public Transform[] waypoints;

	public float transitTime = 2f;

	public AnimationCurve transitCurve;

	public static string defaultUsername = "Awaiting Chat Commands";

	public string username = defaultUsername;

	[SerializeField]
	private Text usernameField;

	[SerializeField]
	private string usernameFormat = "[{0}]";

	private Transform cachedTransform;

	private HasHealth healthSystem;

	private int currentWaypoint = 1;
	private int prevWaypoint = 1;

	private bool inTransit = false;
	private float startTransitTime = 0f;

	//private bool firing = false;

	public Animator animator;
	private Rigidbody2D cachedRigidbody2D;

	private Vector2 forwardFireDirection = Vector2.zero;

	public Transform firePoint;

	public bool dead = false;

	public GameObject explosion;

	public Vector2 getForwardFireDirection() {
		return forwardFireDirection;
	}

	private static EnemyController instance;

	public UIController uiController;

	private Vector3 resetPosition;

	public void StopAllShooting() {
		//Debug.Log("I stopped all shooting!");
		for (int i = firePoint.childCount-1 ; i >=0 ; i-- ){
			GameObject.Destroy(firePoint.transform.GetChild(i).gameObject);
		}
	}

	public void Reset () {
		cachedTransform.position = resetPosition;
		hitSource.Stop();
		deathSource.volume = 1f;
		deathSource.Stop();
		healthSystem.Reset();
		animator.SetBool("Damaged", false);
		animator.SetBool("Firing", false);
		animator.Play("Normal");
		dead = false;
		cachedRigidbody2D.isKinematic = true;
		Collider2D[] colliders = GetComponentsInChildren<Collider2D>();
		for (int i = 0; i < colliders.Length; i++) {
			colliders[i].enabled = true;
		}
		startTransitTime = 0f;
		lastExplosionTime = 0f;
		prevWaypoint = 1;
		currentWaypoint = 1;
		forwardFireDirection = Vector2.down;
		inTransit = false;
		username = defaultUsername;
		UpdateUserName(defaultUsername);

		StopAllCoroutines();
		if (aiControlled) {
			StartCoroutine(AIControl());
		}
	}

	// Use this for initialization
	void Awake () {
		instance = this;
		cachedTransform = GetComponent<Transform>();
		healthSystem = GetComponent<HasHealth>();
		animator = GetComponentInChildren<Animator>();
		cachedRigidbody2D = GetComponent<Rigidbody2D>();
		uiController = GameObject.FindGameObjectWithTag("UI").GetComponent<UIController>();

		resetPosition = cachedTransform.position;
	}

	public static EnemyController getInstance() {
		return instance;
	}

	public bool aiControlled = true;

	[SerializeField]
	private AudioSource hitSource;

	[SerializeField]
	private AudioSource deathSource;

	void Start () {
		Reset();
	}

	public void UpdateUserName (string newUsername) {
		//Debug.Log("UpdateUserName " + newUsername);
		//Debug.Log("aiControlled " + aiControlled);
		if (aiControlled) {
			newUsername = "[BOT] Daemon Kappa";
		}

		username = newUsername;
		usernameField.text = string.Format(usernameFormat, newUsername);
	}

	void OnTriggerEnter2D (Collider2D other) {
		//Debug.Log(gameObject.name + " OnTriggerEnter2D with " + other.gameObject.name);
		ProcessHit(other);
	}

	void OnTriggerStay2D (Collider2D other) {
		//Debug.Log(gameObject.name + " OnTriggerStay2D with " + other.gameObject.name);
		ProcessHit(other);
	}

	void ProcessHit (Collider2D other) {
		/*if (other.gameObject.CompareTag("Enemy")) {
			if (healthSystem.ProcessHit()) {
				healthSystem.Health--;
			}
		}*/
		if (other.gameObject.CompareTag("Bullet")) {
			Bullet bullet = other.gameObject.GetComponent<Bullet>();
			if (!gameObject.CompareTag(bullet.ownerTag) &&
				username != defaultUsername) {
				if (healthSystem.ProcessHit()) {
					SimpleScreenShake.DoScreenShake(0.5f,0.5f,0f,0.1f);
					hitSource.Play();
					bullet.DamageEntity(healthSystem);
				}
			}
		}
	}

	//placeholder ai
	IEnumerator AIControl () {
		while (healthSystem.Health > 0f) {
			yield return new WaitForSeconds(transitTime);

			LanguageInterface.getInstance().loadVolley(username, string.Empty, LanguageInterface.getInstance().getRandomPrefix(),LanguageInterface.getInstance().getRandomSuffix(),Random.Range(0, 20),LanguageInterface.getInstance().getRandomHorizontalPosition(),LanguageInterface.getInstance().getRandomVerticalPosition());
			//animator.SetBool("Firing", true);
			//yield return new WaitForSeconds(1f);
			//animator.SetBool("Firing", false);
			/*int action = Random.Range(0, 6);
			switch (action) {
			case 0:
				MoveUp();
				break;
			case 1:
				MoveDown();
				break;
			case 2:
				MoveLeft();
				break;
			case 3:
				MoveRight();
				break;
			case 4:
				// do nothing
				break;
			}*/
			SetWaypoint(Random.Range(0, waypoints.Length));
		}
	}

	public void SetWaypoint (int wayPoint) {
		if (wayPoint != currentWaypoint &&
			!inTransit) {
			inTransit = true;
			currentWaypoint = wayPoint;
			startTransitTime = Time.time;
		}
	}

	public void MoveUp () {
		if (currentWaypoint > 2 &&
			!inTransit) {
			inTransit = true;
			currentWaypoint -= 3;
			startTransitTime = Time.time;
		}
	}

	public void MoveDown () {
		if (currentWaypoint < 6 &&
			!inTransit) {
			inTransit = true;
			currentWaypoint += 3;
			startTransitTime = Time.time;
		}
	}

	public void MoveLeft () {
		if (currentWaypoint != 0 &&
			currentWaypoint != 3 &&
			currentWaypoint != 6 &&
			!inTransit) {
			inTransit = true;
			currentWaypoint--;
			startTransitTime = Time.time;
		}
	}

	public void MoveRight () {
		if (currentWaypoint != 2 &&
			currentWaypoint != 5 &&
			currentWaypoint != 8 &&
			!inTransit) {
			inTransit = true;
			currentWaypoint++;
			startTransitTime = Time.time;
		}
	}

	void Die () {
		dead = true;
		cachedRigidbody2D.isKinematic = false;
		Collider2D[] colliders = GetComponentsInChildren<Collider2D>();
		for (int i = 0; i < colliders.Length; i++) {
			colliders[i].enabled = false;
		}
		SimpleScreenShake.DoScreenShake(3f,3f,0f,5f);
		StartCoroutine(PlayDeathSound());
		StartCoroutine(uiController.ShowResultsScreen(true));
	}

	float deathStartTime = 0f;
	float deathSoundFadeTime = 5f;

	IEnumerator PlayDeathSound () {
		deathStartTime = Time.time;
		deathSource.Play();
		while (deathSource.volume > 0f) {
			deathSource.volume = Mathf.Lerp(1f, 0f, (Time.time - deathStartTime) / deathSoundFadeTime );
			yield return null;
		}
	}

	private float lastExplosionTime = 0f;
	private float explosionWait = 1f;
	
	// Update is called once per frame
	void Update () {
		bool nowDead = healthSystem.Health <= 0f;
		if (!dead && nowDead) {
			Die();
		}

		if (dead) {
			if (Time.time - lastExplosionTime >= explosionWait) {
				Transform xform = (GameObject.Instantiate(explosion, cachedTransform.position, Quaternion.identity) as GameObject).transform;
				xform.Translate(Random.Range(-1f, 1f), Random.Range(-1f, 0f), 0f);
				xform.localScale = Vector3.one * Random.Range(0.5f, 1.25f);
				lastExplosionTime = Time.time;
				explosionWait = Random.Range(0.1f, 0.3f);
			}
		}

		animator.SetBool("Damaged", dead || !healthSystem.CanBeDamaged());

		if (inTransit && !dead) {
			bool atDestination = Vector3.Distance(cachedTransform.position, waypoints[currentWaypoint].position) <= 0f;

			if (atDestination) {
				inTransit = false;
				prevWaypoint = currentWaypoint;
				//forwardFireDirection = Vector2.zero;
				switch (currentWaypoint) {
				case 0:
					forwardFireDirection = (Vector2.down*2 + Vector2.right).normalized;
					break;
				case 1:
					forwardFireDirection = (Vector2.down).normalized;
					break;
				case 2:
					forwardFireDirection = (Vector2.down*2 + Vector2.left).normalized;
					break;
				case 3:
					forwardFireDirection = (Vector2.right + Vector2.down).normalized;
					break;
				case 4:
					forwardFireDirection = (Vector2.down).normalized;
					break;
				case 5:
					forwardFireDirection = (Vector2.left + Vector2.down).normalized;
					break;
				case 6:
					forwardFireDirection = (Vector2.up + Vector2.right*2).normalized;
					break;
				case 7:
					forwardFireDirection = (Vector2.up).normalized;
					break;
				case 8:
					forwardFireDirection = (Vector2.up + Vector2.left*2).normalized;
					break;
				}
				Debug.DrawRay(transform.position, new Vector3(forwardFireDirection.x, forwardFireDirection.y, 0f), Color.green, 2f);
			}
			else {
				float t = transitCurve.Evaluate((Time.time - startTransitTime) / transitTime);
				cachedTransform.position = Vector3.Lerp(waypoints[prevWaypoint].position, waypoints[currentWaypoint].position, t);
			}
		}
	}

	public void fireVolley(GameObject shooterType, bool isRight, int modifier) {
		GameObject shooter = (GameObject)GameObject.Instantiate(shooterType,transform.position,Quaternion.identity);

		BulletShooter[] shooters = shooter.GetComponents<BulletShooter>();
		for (int i = 0 ; i < shooters.Length ; i++ ) {
			if (isRight) {
				shooters[i].Reverse();
			}
			if (modifier > 0) {
				shooters[i].Modify(modifier);
			}
		}

		shooter.transform.SetParent(this.firePoint.transform);
		shooter.transform.localPosition = Vector3.zero;
		animator.SetBool("Firing",true);
	}
}
