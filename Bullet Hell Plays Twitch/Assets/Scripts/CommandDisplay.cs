﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CommandDisplay : MonoBehaviour {

	private Text text;

	public Color flashColor;

	public Color normalColor;

	public int numFlashes = 5;
	public float flashLength = 1.0f;

	// Use this for initialization
	void Awake () {
		text = GetComponent<Text>();
	}
	
	// Update is called once per frame
	void Update () {
		string newText = LanguageInterface.getInstance().volleyQueue != null && LanguageInterface.getInstance().volleyQueue.Count > 0 ? ((LanguageInterface.Volley)LanguageInterface.getInstance().volleyQueue[0]).originalMessage : string.Empty;

		if (newText != text.text) {
			text.text = newText;
			StartCoroutine(FlashColor());
		}
	}

	IEnumerator FlashColor () {
		for (int i = 0; i < numFlashes; i++) {
			yield return new WaitForSeconds(flashLength);
			text.color = flashColor;
			yield return new WaitForSeconds(flashLength);
			text.color = normalColor;
		}
	}
}