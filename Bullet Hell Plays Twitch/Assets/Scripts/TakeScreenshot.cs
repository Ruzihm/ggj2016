﻿using UnityEngine;
using System.Collections;

public class TakeScreenshot : MonoBehaviour 
{
	public KeyCode screenshotKey = KeyCode.Space;
	public string screenshotFolder = "Screenshots";
	
	void Start () 
	{
		// if the folder doesn't exist, create it.
		if ( !System.IO.Directory.Exists( screenshotFolder ) ) 
		{
			System.IO.Directory.CreateDirectory( screenshotFolder );
		}
	}
	
	// Update is called once per frame
	void Update () 
	{
		if ( Input.GetKeyDown( screenshotKey ) ) 
		{
			System.DateTime screenshotTime = System.DateTime.UtcNow;
			// TODO: format hour, minute, and day correctly.
			string l_screenShotName = screenshotTime.Month + "_" + 
									screenshotTime.Day + "_" + 
									screenshotTime.Year + "__" +
									screenshotTime.Hour + "_" +
									screenshotTime.Minute + "_" +
									screenshotTime.Second + "_shot.png";
			//Debug.Log( "File name test: " + screenshotPath + l_screenShotName );
			Application.CaptureScreenshot( screenshotFolder + "/" + l_screenShotName );
		}
	}
}