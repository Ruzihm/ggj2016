﻿using UnityEngine;
using System.Collections;

public class Bullet : MonoBehaviour {

	Transform cachedTransform;
	SpriteRenderer cachedRenderer;
	Collider2D cachedCollider2D;

	public string ownerTag = string.Empty;

	public float damage = 1f;
	public float life = 12f;

	public float speed = 1f;

	public Vector3 direction = Vector2.down;

	public float acceleration = 1f;

	public bool removeOnImpact = false;
	public bool canOnlyHitOnce = true;
	public bool playerSeeking = false;
	public bool rotates = false;
	public bool explodesOnDeath = false;

	//TODO: use gameobject pool;
	public GameObject explosion;

	// Use this for initialization
	void Awake () {
		cachedRenderer = GetComponentInChildren<SpriteRenderer>();
		cachedCollider2D = GetComponent<Collider2D>();
		cachedTransform = GetComponent<Transform>();
	}

	void LateUpdate () {
		Vector3 bulletMovement;
		if (playerSeeking) {
			Vector3 toPlayerDirection = PlayerController.getInstance().transform.position -
				cachedTransform.position;
			Vector3 currentVel = speed * direction.normalized;
			Vector3 desiredVel = speed * toPlayerDirection.normalized;
			direction = speed * (direction + Vector3.ClampMagnitude(desiredVel - currentVel, acceleration)).normalized;
		}
		bulletMovement = direction;
		if (rotates) {

			cachedRenderer.transform.parent.localRotation = Quaternion.LookRotation(bulletMovement, Vector3.forward);
		}


		bulletMovement.Normalize();
		bulletMovement *= speed;
		bulletMovement *= Time.deltaTime;
		cachedTransform.Translate(bulletMovement);

		life -= Time.deltaTime * speed;
		if (life <= 0) {
			if (explodesOnDeath) {
				GameObject boom = (GameObject)GameObject.Instantiate(explosion);
				boom.transform.position = transform.position;

			}
			GameObject.Destroy(gameObject);
		}
	}

	public void DamageEntity (HasHealth healthHit) {
		//Debug.Log(healthHit.gameObject.name + " hit by " + gameObject.name);
		healthHit.Health -= damage;
		if (removeOnImpact) {
			//TODO: return to pool.
			gameObject.SetActive(false);
			if (explosion != null) {
				GameObject.Instantiate(explosion, cachedTransform.position, Quaternion.identity);
			}
		}
		if (canOnlyHitOnce) {
			cachedCollider2D.enabled = false;
		}
	}
}