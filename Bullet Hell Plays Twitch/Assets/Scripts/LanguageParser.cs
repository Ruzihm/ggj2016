﻿using UnityEngine;
using System.Collections;
using System.Text.RegularExpressions;

/// <summary>
/// Language parser that takes twitch chat input and parses it into our madeup language.
/// </summary>
public class LanguageParser : MonoBehaviour {

	// may need to be public
	private static LanguageParser instance;

  private WindowsTTS tts;


	void Awake () {
		instance = this;
	}

	void OnDestroy () {
		if (instance == this) {
			instance = null;
		}
	}

	public static LanguageParser getInstance() {
		return instance;
	}

	public void ParseChat(string username, string msg) {

		string og_msg = msg;
    msg = msg.ToLower();
		int lefts = 0;
		int rights = 0;
		int ups = 0;
		int downs = 0;
		int exclams = 0;
		string noFluff = Regex.Replace(msg,"v+|[^a-zA-z]+","");

		string pattern = "(k+a+|k+e+r+|b+a+)(p+e+w+|b+l+a+m+|p+o+w+|z+o+k+|z+o+r+t+|p+a+)";

    //string quotePattern = "\"[^\"]+\"";

		Match match = Regex.Match(noFluff, pattern);
	

    //Match quoteMatch = Regex.Match(noFluff, quotePattern);
    if (!match.Success) return;

    //string myQuote = quoteMatch.Value;

    //tts.speak(myQuote);

		string prefix = match.Groups[1].Value;
		string suffix = match.Groups[2].Value;

		//Debug.Log("Prefix: " + prefix);
		//Debug.Log("Suffix: " + suffix);

		LanguageInterface.Prefix prefixType = LanguageInterface.Prefix.CENTER;

		if (Regex.IsMatch(prefix,"k+a+")) {
			prefixType = LanguageInterface.Prefix.LEFT;
		} else if (Regex.IsMatch(prefix,"k+e+r+")) {
			prefixType = LanguageInterface.Prefix.RIGHT;
		} else if (Regex.IsMatch(prefix,"b+a+")) {
			prefixType = LanguageInterface.Prefix.CENTER;
		} else {
			prefixType = LanguageInterface.Prefix.CENTER;
		}

		LanguageInterface.Suffix suffixType = LanguageInterface.Suffix.RANDOM;

		if (Regex.IsMatch(suffix,"p+e+w+")) {
			suffixType = LanguageInterface.Suffix.NORMAL;
		} else if (Regex.IsMatch(suffix,"b+l+a+m")) {
			suffixType = LanguageInterface.Suffix.MINE;
		} else if (Regex.IsMatch(suffix,"p+o+w+")) {
			suffixType = LanguageInterface.Suffix.WAVE;
		} else if (Regex.IsMatch(suffix,"z+o+k+")) {
			suffixType = LanguageInterface.Suffix.SEEKER;
		} else if (Regex.IsMatch(suffix,"z+o+r+t+")) {
			suffixType = LanguageInterface.Suffix.LASER;
		} else if (Regex.IsMatch(suffix,"p+a+")) {
			suffixType = LanguageInterface.Suffix.RANDOM;
		} else {
			suffixType = LanguageInterface.Suffix.RANDOM;
		}


		LanguageInterface.HorizontalPosition hpos = LanguageInterface.HorizontalPosition.CENTER;
		LanguageInterface.VerticalPosition vpos = LanguageInterface.VerticalPosition.CENTER;
		for (int i = 0 ; i < msg.Length ; i ++) {
			if (msg[i] == '<') {
				lefts++; 
			} else if (msg[i] == '>') {
				rights++;
			} else if (msg[i] == 'v') {
				downs++; 
			} else if (msg[i] == '^') {
				ups++;
			} else if (msg[i] == '!') {
				exclams++;
			}
		}

		if (lefts > rights) {
			hpos = LanguageInterface.HorizontalPosition.LEFT;
		} else if (rights > lefts) {
			hpos = LanguageInterface.HorizontalPosition.RIGHT;
		} else {
			hpos = LanguageInterface.HorizontalPosition.CENTER;
		}
			
		if (downs > ups) {
			vpos = LanguageInterface.VerticalPosition.BOTTOM;
		} else if (ups > downs) {
			vpos = LanguageInterface.VerticalPosition.TOP;
		} else {
			vpos = LanguageInterface.VerticalPosition.CENTER;
		}
			
		LanguageInterface.getInstance().loadVolley(username, og_msg, prefixType,suffixType,exclams,hpos,vpos);
	}
}
