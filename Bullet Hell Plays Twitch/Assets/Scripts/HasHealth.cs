﻿using UnityEngine;
using System.Collections;

public class HasHealth : MonoBehaviour {

	private UIBar healthBar;

	public float hitCooldownTime = 1f;
	private float lastHitTime = 0f;

	public float maxHealth = 100f;

	private float currHealth = 100f;

	#if UNITY_EDITOR
	void OnMouseDown ()
	{
		print("setting health to 0");

		this.Health = -1.0f;

		//SceneManager.LoadScene("Scenes/game", LoadSceneMode.Single);
	}
	#endif

  public float Health {
		get {
			return currHealth;
		}
		set {
			currHealth = Mathf.Clamp(value, 0f, maxHealth);
			healthBar.FillTarget = (currHealth / maxHealth);

			/*if (currHealth <= 0) {
				this.GameOver();
			}*/
		}
	}

	public void Reset () {
		Health = maxHealth;
		healthBar.Reset();
		healthBar.FillTarget = (currHealth / maxHealth);
	}

	// Use this for initialization
	void Awake () {
		healthBar = GetComponentInChildren<UIBar>();
	}

	void Start () {
		Reset();
	}
	
	public bool ProcessHit () {
		if (CanBeDamaged()) {
			lastHitTime = Time.time;
			return true;
		}
		return false;
	}

	public bool CanBeDamaged () {
		return lastHitTime == 0f || Time.time - lastHitTime >= hitCooldownTime;
	}
}
