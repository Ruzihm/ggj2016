﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Language interface that has set of public functions that control facets of the game.
/// </summary>
public class LanguageInterface : MonoBehaviour {

	public class Volley {
		public string username;
		public string originalMessage;
		public Prefix prefix;
		public Suffix suffix;
		public int power;
		public HorizontalPosition hpos;
		public VerticalPosition vpos;

		public Volley(string username, string originalMessage, Prefix prefix, Suffix suffix, int power, HorizontalPosition hpos, VerticalPosition vpos) {
			this.username = username;
			this.originalMessage = originalMessage;
			this.prefix = prefix;
			this.suffix = suffix;
			this.power = power;
			if (power > maxPower) { 
				power = maxPower;
			}
			this.hpos = hpos;
			this.vpos = vpos;
		}

		public override string ToString() {
			return username + ":" + prefix.ToString() + ":" + suffix.ToString() + ":" + power +
				hpos.ToString() + ":" +vpos.ToString();
		}
	}

	public enum Prefix {
		LEFT, 
		RIGHT, 
		CENTER
	};

	public Prefix getRandomPrefix () {
		Prefix[] prefixes = {
			Prefix.LEFT,
			Prefix.RIGHT,
			Prefix.CENTER
		};

		return prefixes[Random.Range(0,prefixes.Length)];
	}

	public enum Suffix {
		NORMAL,
		LASER,
		MINE,
		SEEKER,
		WAVE,
		RANDOM
	};

	public Suffix getRandomSuffix() {
		Suffix[] suffixes = {
			Suffix.NORMAL,
			Suffix.LASER,
			Suffix.MINE,
			Suffix.SEEKER,
			Suffix.WAVE
		};

		return suffixes[Random.Range(0,suffixes.Length)];
	}

	public enum HorizontalPosition {
		LEFT,
		CENTER,
		RIGHT
	};

	public HorizontalPosition getRandomHorizontalPosition() {
		HorizontalPosition[] options = {
			HorizontalPosition.LEFT,
			HorizontalPosition.CENTER,
			HorizontalPosition.RIGHT
		};

		return options[Random.Range(0, options.Length)];
	}

	public enum VerticalPosition {
		TOP,
		CENTER,
		BOTTOM
	};

	public VerticalPosition getRandomVerticalPosition() {
		VerticalPosition[] options = {
			VerticalPosition.TOP,
			VerticalPosition.CENTER,
			VerticalPosition.BOTTOM
		};

		return options[Random.Range(0, options.Length)];
	}

	public Hashtable submittedVolleys = new Hashtable();
	public ArrayList volleyQueue = new ArrayList();

	public const int maxPower = 200;

	public float sampleTimeWindow = 5f; 
	public float sampleTimer = 0f;
	public int startingVolleyCount = 1;
	public bool isPreGame = true;

	private static LanguageInterface instance;

	private EnemyController enemyControl;

	private UIController uiControl;

	void Awake () {
		instance = this;
	}

	void OnDestroy () {
		if (instance == this) {
			instance = null;
		}
	}

	// Use this for initialization
	void Start () {
		enemyControl = FindObjectOfType<EnemyController>();
		uiControl = GameObject.FindGameObjectWithTag("UI").GetComponent<UIController>();

	}

	public void Renew() {
		submittedVolleys = new Hashtable();
		volleyQueue = new ArrayList();
		isPreGame = true;
	}
		

	void Update() {
		sampleTimer += Time.deltaTime;
		bool volleyLoaded = false;
		if (sampleTimer > sampleTimeWindow) {
			if (!isPreGame) {
				sampleTimer = 0f;
				if (submittedVolleys.Count == 0) {
					// repeat previous volley
					volleyQueue.Add(volleyQueue[volleyQueue.Count-1]);
				} else {
					sampleVolley();
				}
				volleyQueue.RemoveAt(0);
				volleyLoaded = true;
				beginNextVolley();
			} else {
				if (submittedVolleys.Count > 0) {
					volleyLoaded = true;
					sampleTimer = 0f;
					sampleVolley();

					if (volleyQueue.Count >= startingVolleyCount) {
						startGame();
					}
				}
			}
		}

		if (volleyLoaded) {
			//Debug.Log("Volley loaded: " + ((Volley)volleyQueue[volleyQueue.Count-1]).ToString());
			//Debug.Log("Volley count: " + volleyQueue.Count);
		}
	}
	private void prepAndStartVolley() {
		
	}

	private void startGame() 
	{
		//Debug.Log("Starting game!!");
		//TODO: Start Game Stuff!

		isPreGame = false;

		beginNextVolley();
	}


	public static LanguageInterface getInstance() {
		return instance;
	}

	// template function design.
	public void loadVolley (string username, string message, Prefix prefix, Suffix baseBlast, int suffixCount, HorizontalPosition hpos, VerticalPosition vpos) {
		Volley vol = new Volley(username, message, prefix, baseBlast, suffixCount, hpos, vpos);
		submittedVolleys[username] = vol;
	}

	/* 
	 * Once one-more-volley submission time is over, run this method 
	 * to figure out which volley will be available for the boss
	 * and add it to availableVolleys
	 */
	public void sampleVolley() {
		Volley selectedVolley;

		ArrayList allUserList = new ArrayList();
		foreach(string submittedUsername in submittedVolleys.Keys) {
			allUserList.Add(submittedUsername);
		}

		string selectedUser = (string) allUserList[Random.Range(0,allUserList.Count)];

		selectedVolley = (Volley) submittedVolleys[selectedUser];
		volleyQueue.Add(selectedVolley);

		submittedVolleys = new Hashtable();
	}

	/* 
	 * Call this method when it's time to begin the next volley
	 */
	public void beginNextVolley() {
		Volley currentVolley = (Volley) volleyQueue[0];

		//Debug.Log("Firing volley: " + currentVolley);
		//Debug.Log(enemyControl);
		enemyControl.UpdateUserName(currentVolley.username);
		GameObject shooterType = null;

		Suffix resultSuffix = currentVolley.suffix;
		if (currentVolley.suffix == Suffix.RANDOM) {
			resultSuffix = getRandomSuffix();
		}
			
		switch (resultSuffix) {
		case Suffix.NORMAL:
			if (currentVolley.prefix == Prefix.CENTER) {
				shooterType = enemyControl.Shooter_Center_Normal; 
			} else {
				shooterType = enemyControl.Shooter_Left_Normal;
			}
			break;
		case Suffix.WAVE:
			if (currentVolley.prefix == Prefix.CENTER) {
				shooterType = enemyControl.Shooter_Center_Wave; 
			} else {
				shooterType = enemyControl.Shooter_Left_Wave;
			}
			break;
		case Suffix.MINE:
			if (currentVolley.prefix == Prefix.CENTER) {
				shooterType = enemyControl.Shooter_Center_Mine; 
			} else {
				shooterType = enemyControl.Shooter_Left_Mine;
			}
			break;
		case Suffix.SEEKER:
			if (currentVolley.prefix == Prefix.CENTER) {
				shooterType = enemyControl.Shooter_Center_Seeker; 
			} else {
				shooterType = enemyControl.Shooter_Left_Seeker;
			}
			break;
		case Suffix.LASER:
			if (currentVolley.prefix == Prefix.CENTER) {
				shooterType = enemyControl.Shooter_Center_Laser; 
			} else {
				shooterType = enemyControl.Shooter_Left_Laser;
			}
			break;
		default:
			break;
		}

		if (currentVolley.power > 3 ) currentVolley.power = 3;

		if (null != shooterType) {
			enemyControl.fireVolley(shooterType, Prefix.RIGHT == currentVolley.prefix, currentVolley.power);
		}

		int moveIndex = 0;
		if (currentVolley.hpos == HorizontalPosition.CENTER) {
			moveIndex += 1;
		} else if (currentVolley.hpos == HorizontalPosition.RIGHT) {
			moveIndex += 2;
		}

		if (currentVolley.vpos == VerticalPosition.CENTER) {
			moveIndex += 3;
		} else if (currentVolley.vpos == VerticalPosition.BOTTOM) {
			moveIndex += 6;
		}

		enemyControl.SetWaypoint(moveIndex);
	}
}
