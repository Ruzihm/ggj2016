﻿using UnityEngine;
using System.Collections;

public class IRCGUI : MonoBehaviour {

	private Rect windowRect = new Rect ((Screen.width - 200)/2, (Screen.height - 130)/2,200,130);
	private bool show = false;
	string oauth = "";
	string username = "";
	public TwitchIRC irc;

	void OnGUI ()
	{
		if (show)
			windowRect = GUI.Window(0,windowRect, DialogWindow, "Twitch Options");
	}

	void DialogWindow (int windowID)
	{
		float y = 20;
		GUI.Label(new Rect(5, y, windowRect.width-10, 20), "oauth");
		oauth = GUI.TextField(new Rect(5,y+20, windowRect.width-10, 20),oauth);
		GUI.Label(new Rect(5, y+40, windowRect.width-10, 20), "username");
		username = GUI.TextField(new Rect(5,y+60, windowRect.width-10, 20),username);
		if (GUI.Button(new Rect(5,y+80, windowRect.width-10, 20), "Play"))
		{
			if (oauth.Length > 0) 
			{
				irc.oauth = oauth;
				irc.nickName = username;
				irc.channelName = username;
			}

			irc.enabled = true;
			show = false;
		}
	}

	public void Open()
	{
		show = true;
	}

	// Use this for initialization
	void Start () {
		this.Open();
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
