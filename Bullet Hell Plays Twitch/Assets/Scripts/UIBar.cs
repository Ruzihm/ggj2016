﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UIBar : MonoBehaviour {

	[SerializeField]
	private float barMaxSpeed = 0.025f;

	private float target = 0f;

	private CanvasGroup barGroup;

	[SerializeField]
	private Image fill;

	[SerializeField]
	private Image bg;

	//TODO: bar fades in and out when no damage.

	public float FillTarget {
		get {
			return target;
		}
		set {
			target = Mathf.Clamp(value, 0f, 1f);
		}
	}

	[SerializeField]
	private float startFillAmount = 1f;

	//private float lastStillTime = 0f;

	void Awake () {
		barGroup = GetComponent<CanvasGroup>();
	}

	void Start () {
		Reset();
	}

	public void Reset () {
		target = startFillAmount;
		fill.fillAmount = startFillAmount;
	}

	void Update () {
		fill.fillAmount = Mathf.MoveTowards(fill.fillAmount, target, barMaxSpeed);
		/*if (fill.fillAmount == target) {
			lastStillTime = Time.time;
		}
		else {
			
		}*/
	}
}
