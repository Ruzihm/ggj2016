﻿using UnityEngine;
using System.Collections;

public class BulletShooter : MonoBehaviour {

	public float fireRate = 1f;
	public float executeRate = 1f;
	public float delay = 0f;
	public bool closer = false;
	public GameObject bulletToSpawn;
	public float speedModifier = 1f;
	public int damageModifier = 0;

	// Aim direction - down = opposite nondrant of screen
	public Vector2 startDirection = Vector2.down;
	public Vector2 finishDirection = Vector2.down;
	public AnimationCurve rotationcurve;


	float time = 0f;
	float shoottime = 0f;


	// Use this for initialization
	void Start () {
		startDirection.Normalize();
		finishDirection.Normalize();
	}


	// Update is called once per frame
	void Update () {
		
		time += Time.deltaTime * executeRate* speedModifier;

		if (delay > 0) {
			delay -= Time.deltaTime * executeRate * speedModifier;
			return;
		}

		shoottime += Time.deltaTime * executeRate * speedModifier;

		// Adjust shooting direction
		Quaternion fullRotation = Quaternion.FromToRotation(startDirection,finishDirection);
		Quaternion partialRotation = Quaternion.Lerp(Quaternion.identity,fullRotation,rotationcurve.Evaluate(time));
		Vector3 relativeDirection = partialRotation * startDirection;
		relativeDirection.Normalize();

		Quaternion relativeRotation = Quaternion.FromToRotation(Vector3.down, relativeDirection);


		Vector3 direction = relativeRotation * EnemyController.getInstance().getForwardFireDirection();

		direction.Normalize();

		while (shoottime >= fireRate) {
			Shoot(direction);
			shoottime -= fireRate;
		}

		if (time >= rotationcurve.keys[rotationcurve.length-1].time) {
			if (closer) {
				EnemyController.getInstance().animator.SetBool("Firing",false);
				Destroy(gameObject);
			} else {
				enabled=false;
			}
		}
	}

	void Shoot(Vector2 direction) {
		Bullet newBullet = ((GameObject)GameObject.Instantiate(bulletToSpawn,transform.position,Quaternion.identity)).GetComponent<Bullet>();
		newBullet.direction = direction;
		newBullet.damage += damageModifier;
		newBullet.speed *= speedModifier;
	}

	public void Reverse() {
		this.startDirection.x *= -1f;
		this.finishDirection.x *= -1f;;
	}


	public void Modify(int modifier) {
		damageModifier = modifier;
		speedModifier = Mathf.Pow(0.95f,modifier);
	}
}
