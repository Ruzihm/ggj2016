﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PlayerController : MonoBehaviour {

	private Rigidbody2D cachedRigidBody2D;
	private BoxCollider2D cachedBoxCollider2D;
	private HasHealth healthSystem;
	private Animator animator;

	private Transform bulletsParent;

	public float speed = 100f;

	public float collisionDamage = 2f;

	private static PlayerController instance;

	private Transform cachedTransform;
	public UIController uiController;
	public EnemyController enemyController;

	//TODO: game object pool will probably be necessary.
	[SerializeField]
	private GameObject bulletPrefab;

	[SerializeField]
	private Transform firePoint;

	private Vector3 resetPosition;

	public bool aiControlled = false;

	//public AudioClip fireClip;

	// Use this for initialization
	void Awake () {
		instance = this;
		cachedTransform = GetComponent<Transform>();
		cachedRigidBody2D = GetComponent<Rigidbody2D>();
		cachedBoxCollider2D = GetComponent<BoxCollider2D>();
		healthSystem = GetComponent<HasHealth>();
		animator = GetComponentInChildren<Animator>();
		cachedRigidbody2D = GetComponent<Rigidbody2D>();
		bulletsParent = GameObject.FindGameObjectWithTag("BulletsParent").transform;

		enemyController = FindObjectOfType<EnemyController>();
		uiController = GameObject.FindGameObjectWithTag("UI").GetComponent<UIController>();

		resetPosition = cachedTransform.position;
	}

	public void Reset () {
		cachedTransform.position = resetPosition;
		lastExplosionTime = 0f;
		deathSource.Stop();
		deathSource.volume = 0.5f;
		hitSource.Stop();
		healthSystem.maxHealth = aiControlled ? 200f : 25f;
		healthSystem.Reset();
		animator.SetBool("Damaged", false);
		animator.SetBool("Firing", false);
		animator.Play("PlayerNormal");

		dead = false;
		cachedRigidBody2D.gravityScale = 0.0f;
		Collider2D[] colliders = GetComponentsInChildren<Collider2D>();
		for (int i = 0; i < colliders.Length; i++) {
			colliders[i].enabled = true;
		}

		UpdateUserName(FindObjectOfType<TwitchIRC>().nickName);
	}

	public static PlayerController getInstance () {
		return instance;
	}

	private float lastExplosionTime = 0f;
	private float explosionWait = 1f;
	
	// Update is called once per frame
	void Update () {
		bool nowDead = healthSystem.Health <= 0f;

		if (!dead && nowDead) {
			Die();
		}

		animator.SetBool("Damaged", dead || !healthSystem.CanBeDamaged());

		if (dead) {
			if (Time.time - lastExplosionTime >= explosionWait) {
				Transform xform = (GameObject.Instantiate(explosion, cachedTransform.position, Quaternion.identity) as GameObject).transform;
				xform.Translate(Random.Range(-1f, 1f), Random.Range(-1f, 0f), 0f);
				xform.localScale = Vector3.one * Random.Range(0.5f, 1.25f);
				lastExplosionTime = Time.time;
				explosionWait = Random.Range(0.1f, 0.3f);
			}
		}

		if (!dead) {
			//GetAxisRaw or GetAxis
			//Mathf.PerlinNoise(Time.time, 0f);
			float verticalInput = aiControlled ? Mathf.PerlinNoise(Time.time, 0.5f) * 2f - 1f : Input.GetAxis("Vertical");
			float horizontalInput = aiControlled ? Mathf.PerlinNoise(Time.time, 2.5f) * 2f - 1f : Input.GetAxis("Horizontal");

			Vector2 playerMovement;
			playerMovement.x = horizontalInput;
			playerMovement.y = verticalInput;
			if (playerMovement.magnitude > 1f) {
				playerMovement.Normalize();
			}
			//Debug.Log(playerMovement);
			playerMovement *= speed;
			//Debug.Log(playerMovement);
			cachedRigidBody2D.velocity = playerMovement;

			//Debug.DrawRay(cachedTransform.position, Vector3.up, Color.red, 2f);

			RaycastHit2D hit;
			if (aiControlled) {
				hit = Physics2D.Raycast(cachedTransform.position, Vector2.up, 15f, LayerMask.NameToLayer("Enemy"));
				//Debug.Log(hit.collider.gameObject.name);
			}

			bool shouldFire = aiControlled ? hit.collider.gameObject.CompareTag("Enemy") && EnemyController.getInstance().username != EnemyController.defaultUsername : Input.GetButton("Jump");

			if (shouldFire &&
				Time.time - lastFireTime >= fireCooldown) {
				FireBullet();
			}
			else if (!shouldFire) {
				animator.SetBool("Firing", false);
			}
			//cachedRigidBody2D.AddForce(force);
			//Debug.Log("horz " + horizontalInput);
		}
	}

	[SerializeField]
	private float fireCooldown = 0.1f;
	private float lastFireTime = 0f;

	[SerializeField]
	private AudioSource fireSource;

	[SerializeField]
	private AudioSource hitSource;

	void FireBullet () {
		animator.SetBool("Firing", true);
		fireSource.Play();
		//AudioSource.PlayClipAtPoint(fireClip, Vector3.zero);
		Bullet newBullet = (GameObject.Instantiate(bulletPrefab, firePoint.position, Quaternion.identity) as GameObject).GetComponent<Bullet>();
		newBullet.transform.SetParent(bulletsParent);
		lastFireTime = Time.time;
	}

	//TODO: not happening on staying in enemy.

	void OnTriggerEnter2D (Collider2D other) {
		//Debug.Log(gameObject.name + " OnTriggerEnter2D with " + other.gameObject.name);
		ProcessHit(other);
	}

	void OnTriggerStay2D (Collider2D other) {
		//Debug.Log(gameObject.name + " OnTriggerStay2D with " + other.gameObject.name);
		ProcessHit(other);
	}

	[SerializeField]
	private Text usernameField;

	[SerializeField]
	private string usernameFormat = "[{0}]";

	public void UpdateUserName (string newUsername) {
		if (newUsername == "twitchgoestobullethell") {
			newUsername = "OSFrog";
		}
		if (aiControlled) {
			newUsername = "[BOT] " + newUsername;
		}
		usernameField.text = string.Format(usernameFormat, newUsername);
	}

	public bool dead = false;

	public GameObject explosion;

	private Rigidbody2D cachedRigidbody2D;

	void Die () {
		dead = true;
		cachedRigidBody2D.gravityScale = 0.5f;
		Collider2D[] colliders = GetComponentsInChildren<Collider2D>();
		for (int i = 0; i < colliders.Length; i++) {
			colliders[i].enabled = false;
		}
		SimpleScreenShake.DoScreenShake(2f,2f,0f,5f);
		StartCoroutine(PlayDeathSound());
		StartCoroutine(uiController.ShowResultsScreen(false));
	}

	[SerializeField]
	private AudioSource deathSource;

	float deathStartTime = 0f;
	float deathSoundFadeTime = 5f;

	IEnumerator PlayDeathSound () {
		deathStartTime = Time.time;
		deathSource.Play();
		while (deathSource.volume > 0f) {
			deathSource.volume = Mathf.Lerp(0.5f, 0f, (Time.time - deathStartTime) / deathSoundFadeTime );
			yield return null;
		}
	}

	void ProcessHit (Collider2D other) {
		float damage = 0f;
		if (other.gameObject.CompareTag("Enemy")) {
			if (EnemyController.getInstance().username != EnemyController.defaultUsername) {
				if (healthSystem.ProcessHit()) {
					SimpleScreenShake.DoScreenShake(1f, 1f, 0f, 0.5f);
					hitSource.Play();
					healthSystem.Health -= collisionDamage;
					damage = collisionDamage;
				}
			}
		}
		if (other.gameObject.CompareTag("Bullet")) {
			Bullet bullet = other.gameObject.GetComponent<Bullet>();
			if (!gameObject.CompareTag(bullet.ownerTag)) {
				if (healthSystem.ProcessHit()) {
					SimpleScreenShake.DoScreenShake(2f, 2f, 0f, 0.2f);
					hitSource.Play();
					bullet.DamageEntity(healthSystem);
					damage = bullet.damage;
				}
			}
		}
		if (damage > 0f) {
			uiController.AddScoreEntry(enemyController.username, damage);
		}
	}
}
