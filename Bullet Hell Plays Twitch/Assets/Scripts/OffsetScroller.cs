﻿using UnityEngine;
using System.Collections;

public class OffsetScroller : MonoBehaviour {

	[Range(0f,4f)]
	public float scrollSpeed;
	public float originalScrollSpeed;
	private Vector2 savedOffset;
	private Renderer cachedRenderer;

	void Start () {
		originalScrollSpeed = scrollSpeed;
		cachedRenderer = GetComponent<Renderer>();
		savedOffset = cachedRenderer.sharedMaterial.GetTextureOffset ("_MainTex");
	}

	void Update () {
		if (scrollSpeed != 0) {
			float y = Mathf.Repeat (Time.time * scrollSpeed, 1);
			Vector2 offset = new Vector2 (savedOffset.x, y);
			cachedRenderer.sharedMaterial.SetTextureOffset ("_MainTex", offset);
		}
	}

	void OnDisable () {
		cachedRenderer.sharedMaterial.SetTextureOffset ("_MainTex", savedOffset);
	}
}