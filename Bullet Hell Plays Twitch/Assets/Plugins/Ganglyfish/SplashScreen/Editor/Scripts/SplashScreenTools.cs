﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;

public static class SplashScreenTools {
	
	/// The asset path to the splash screen scene.
	private static string scenePath = "Assets/Plugins/Ganglyfish/SplashScreen/Scenes/SplashScreen.unity";

	private static string tagToAdd = "SplashScreen";

	[MenuItem("Tools/Ganglyfish/Add Splash Screen")]
	/// <summary>
	/// Adds the splash screen scene and tag to the build.
	/// </summary>
	public static void AddSplashScreenToBuild () {

		// Add the splash screen scene.
		InsertFirstScene(scenePath);
		
		// Add the splash screen tag.
		AddTag(tagToAdd);
	}

	public static void InsertFirstScene (string newScenePath) {
		// Get a list of scenes.
		List<EditorBuildSettingsScene> updatedScenes = new List<EditorBuildSettingsScene>(EditorBuildSettings.scenes);
		
		// The scene needs to be added if there are no scenes or it is not the first scene.
		if (updatedScenes.Count < 1 || updatedScenes[0].path != newScenePath) {
			// Completely add the new scene from scratch.
			EditorBuildSettingsScene newScene = new EditorBuildSettingsScene(newScenePath, true);
			updatedScenes.Insert(0, newScene);
		}
		// Check if the scene needs to be enabled.
		else if (!updatedScenes[0].enabled) {
			// Enable the scene in the build settings.
			updatedScenes[0].enabled = true;
		}
		else {
			Debug.Log("Scene already added and enabled.");
		}
		
		// Apply the scene changes (if any).
		EditorBuildSettings.scenes = updatedScenes.ToArray();
	}

	public static void AddTag (string tagName) {
		// Find the TagManager asset.
		UnityEngine.Object[] asset = AssetDatabase.LoadAllAssetsAtPath("ProjectSettings/TagManager.asset");
		if ((asset != null) && (asset.Length > 0)) {
			SerializedObject so = new SerializedObject(asset[0]);
			SerializedProperty tags = so.FindProperty("tags");

			for (int i = 0; i < tags.arraySize; i++) {
				if (tags.GetArrayElementAtIndex(i).stringValue == tagName) {
					// Tag already exists, abort.
					Debug.Log("Tag already exists.");
					return;
				}
			}

			// Insert the new tag.
			tags.InsertArrayElementAtIndex(0);
			tags.GetArrayElementAtIndex(0).stringValue = tagName;
			so.ApplyModifiedProperties();
			so.Update();
		}
	}
}