﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace Ganglyfish {

	/// <summary>
	/// Controller for the splash screen.
	/// Handles an intro, hold, and outro period.
	/// </summary>
	public class SplashScreen : MonoBehaviour {

		// All of the display objects for the splash screen.
		private CanvasGroup screenDisplayGroup;

		// Transform for the Text on the splash screen.
		private Transform holdTextTransform;

		// Reference to listener to avoid duplicates when next scene is loaded in.
		private AudioListener screenListener;

		// AudioSource that will play the logo sound.
		private AudioSource holdSource;

		[Header("Intro")]

		// Display curve for tweening in splash screen.
		[SerializeField]
		private AnimationCurve introAlphaCurve;

		[Header("Hold")]

		// Display curve for scaling the text while holding during the splash screen.
		[SerializeField]
		private AnimationCurve holdTextScaleCurve;

		// Delay before AudioClip is played.
		[SerializeField]
		private float holdAudioDelay = 0.25f;
		
		// Volume the AudioClip will play at.
		[SerializeField]
		private float holdAudioVolume = 0.1f;

		//[SerializeField]
		//private float holdAnimationDelay = 0.25f;
		
		// Amount of time that the logo screen is held on.
		[SerializeField]
		private float holdTime = 1f;
		
		// Index of level to be loaded when Splash Screen has faded in.
		[SerializeField]
		private int levelToLoad = 1;

		// Possible methods for triggering the Splash Screen outro.
		private enum OutroTrigger { Automatic, Manual };

		[Header("Outro")]

		// What method is used to trigger the outro.
		[SerializeField]
		private OutroTrigger outroTrigger;

		// Display curve for tweening out splash screen.
		[SerializeField]
		private AnimationCurve outroAlphaCurve;

		// Phases that the Splash Screen can be in.
		private enum ScreenPhase { Intro, Hold, Outro };

		// The current screen phase.
		private ScreenPhase currentPhase;

		// Type of delegate that takes no parameters and returns nothing.
		public delegate void OnVoidDelegate();

		// Delegate that fires when the splash screen is disposed of.
		public static OnVoidDelegate onDispose;

		// Tag that identifies the splash screen GameObject.
		public static string gameObjectTag = "SplashScreen";

		// Tracks time the current phase started.
		private float phaseStartTime;

		// Has the call to load the next scene been made? 
		private bool nextSceneLoadTriggered;

		// Has the call to destroy this GameObject been made?
		private bool destructionTriggered;

		/// <summary>
		/// Sets up the splash screen.
		/// </summary>
		private void Awake () {
			// Grab necessary component references.
			screenDisplayGroup = GetComponentInChildren<CanvasGroup>();
			screenListener = GetComponent<AudioListener>();
			holdSource = GetComponent<AudioSource>();
			holdTextTransform = GetComponentInChildren<Text>().transform;

			// Make sure that this doesn't get destroyed when the next scene is loaded.
			DontDestroyOnLoad(gameObject);

			// Tag this object so that other scripts can access it.
			gameObject.tag = gameObjectTag;

			// Reset control and display variables.
			currentPhase = ScreenPhase.Intro;
			phaseStartTime = Time.time;
			nextSceneLoadTriggered = false;
			destructionTriggered = false;
		}
		
		/// <summary>
		/// Updates according to the state of the splash screen.
		/// </summary>
		private void Update () {
			switch (currentPhase) {

			case ScreenPhase.Intro:
				// Fade in the screen.
				screenDisplayGroup.alpha = introAlphaCurve.Evaluate(Time.time - phaseStartTime);
				if (screenDisplayGroup.alpha == 1f) {
					AdvancePhase();
					// Start the hold audio.
					if (holdSource.clip != null) {
						holdSource.volume = holdAudioVolume;
						holdSource.PlayDelayed(holdAudioDelay);
					}
				}
				break;

			case ScreenPhase.Hold:
				// Check the time to hold.
				holdTextTransform.localScale = Vector3.one * holdTextScaleCurve.Evaluate(Time.time - phaseStartTime);
				if (Time.time - phaseStartTime >= holdTime && !nextSceneLoadTriggered) {
					// Load the next scene.
					nextSceneLoadTriggered = true;
					Application.LoadLevel(levelToLoad);
				}
				break;

			case ScreenPhase.Outro:
				// Fade out the screen.
				screenDisplayGroup.alpha = outroAlphaCurve.Evaluate(Time.time - phaseStartTime);
				if (screenDisplayGroup.alpha == 0f && !destructionTriggered) {
					// Destroy this GameObject.
					destructionTriggered = true;

					// Fire dispose delegate.
					if (onDispose != null) {
						onDispose();
					}

					Destroy(gameObject);
				}
				break;
			}
		}

		/// <summary>
		/// Handles when a new level is loaded.
		/// </summary>
		/// <param name="level">The index of the level that was loaded.</param>
		private void OnLevelWasLoaded (int level) {
			// If the level being loaded is the level that the splash screen loads after holding.
			if (level == levelToLoad) {
				// Destroy the now duplicate AudioListener.
				Destroy(screenListener);
				screenListener = null;
				
				// Check for automatically playing the outro.
				if (outroTrigger == OutroTrigger.Automatic) {
					AdvancePhase();
				}
			}
		}

		/// <summary>
		/// Advance to the next phase of the splash screen. 
		/// </summary>
		private void AdvancePhase () {
			currentPhase++;
			phaseStartTime = Time.time;
		}

		/// <summary>
		/// Manually trigger the outro for the splash screen.
		/// </summary>
		public void TriggerOutro () {
			if (outroTrigger == OutroTrigger.Manual) {
				AdvancePhase();
			}
		}
	}
}